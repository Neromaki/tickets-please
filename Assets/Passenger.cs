﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Passenger : MonoBehaviour {

    public bool ticketChecked = false;
    public bool problemTicket = false;
    public string ticketOrigin;
    public string ticketDestination;
    public string ticketDate;
    public string ticketTime;
    GameObject gameController;
    GameObject player;

    GameObject uiTicketOrigin;


    

    [SerializeField] float chanceOfIssue = 0.4f;

	// Use this for initialization
	void Start ()
    {
        gameController = GameObject.FindWithTag("GameController");
        player = GameObject.FindWithTag("Player");
    }

    private void generateIssue()
    {
        float diceRoll = Random.Range(0.0f, 1.0f);
        Debug.Log("DICE:" + diceRoll);
        Debug.Log("chanceOfIssue:" + chanceOfIssue);
        if (diceRoll <= chanceOfIssue)
        {
            problemTicket = true;
            float issueRoll = Random.Range(0.0f, 1.0f);
            Debug.Log("TICKET: issueRoll of " + issueRoll.ToString());
            if (issueRoll < 0.2f)
            {
                Debug.Log("TICKET: Changing ORIGIN");
                var origins = gameController.GetComponent<GameController>().origins;
                gameController.GetComponent<GameController>().setTicketData("Origin", origins[Random.Range(0, origins.Length - 1)]);
            }
            else if (issueRoll < 0.4f)
            {
                Debug.Log("TICKET: Changing DESTINATION");
                var destinations = gameController.GetComponent<GameController>().destinations;
                gameController.GetComponent<GameController>().setTicketData("Destination", destinations[Random.Range(0, destinations.Length - 1)]);
            }
            else if (issueRoll < 0.6f)
            {
                Debug.Log("TICKET: Changing DATE");
                var date = gameController.GetComponent<GameController>().generateDate();
                gameController.GetComponent<GameController>().setTicketData("Date", date);
            }
            else if (issueRoll < 0.8f)
            {
                Debug.Log("TICKET: Changing TIME");
                var time = gameController.GetComponent<GameController>().generateTime();
                gameController.GetComponent<GameController>().setTicketData("Time", time);
            }
        }
    }

    // Update is called once per frame
    void Update () {
        if (ticketChecked)
        {
            gameObject.transform.Find("Context").GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            var contextIcon = gameObject.transform.Find("Context");
            if (contextIcon.GetComponent<SpriteRenderer>().enabled)
            {
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    Debug.Log("CHECKING TICKET");
                    player.GetComponentInParent<CharacterController>().enabled = false;
                    gameController.GetComponent<GameController>().toggleTrainTicket(true);
                    gameController.GetComponent<GameController>().passengerBeingInspected = gameObject;
                    generateIssue();
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && !ticketChecked)
        {
            var contextIcon = gameObject.transform.Find("Context");
            contextIcon.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            var contextIcon = gameObject.transform.Find("Context");
            contextIcon.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

}
