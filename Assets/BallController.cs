﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    private bool ballIsActive;
    private Vector3 ballPosition;
    private Vector2 ballInitialForce;
    public float constantSpeed = 1.0f;

    // GameObject
    public GameObject playerObject;
    private Rigidbody2D rigidBody;

    // Use this for initialization
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        // create the force
        ballInitialForce = new Vector2(350.0f, 500.0f);

        // set to inactive
        ballIsActive = false;

        // ball position
        ballPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        // check for user input
        if (Input.GetButtonDown("Jump") == true)
        {
            // check if is the first play
            if (!ballIsActive)
            {
                // reset the force
                rigidBody.isKinematic = false;

                // add a force
                rigidBody.AddForce(ballInitialForce);

                // set ball active
                ballIsActive = !ballIsActive;
            }
        }

        if (!ballIsActive && playerObject != null)
        {

            // get and use the player position
            ballPosition.x = playerObject.transform.position.x;

            // apply player X position to the ball
            transform.position = ballPosition;
        }

        // Check if ball falls
        if (ballIsActive && transform.position.y < -6)
        {
            ballIsActive = !ballIsActive;
            ballPosition.x = playerObject.transform.position.x;
            ballPosition.y = -4.2f;
            transform.position = ballPosition;

            rigidBody.isKinematic = true;
        }

        
    }

    private void LateUpdate()
    {
        if(ballIsActive)
        {
            rigidBody.velocity = 12.0f * rigidBody.velocity.normalized;
        }

    }
}