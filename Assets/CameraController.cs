﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    GameObject player;
    private Vector3 offset;

    void Start()
    {
        player = GameObject.FindWithTag("PlayerParent");
        offset = transform.position - player.transform.position;
    }


    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }
}