﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExteriorScroller : MonoBehaviour {

    public float scrollSpeed = 0.5f;


    void Start ()
    {

    }

    void Update ()
    {
        Vector2 offset = new Vector2(Time.time * scrollSpeed, 0f);
        GetComponent<Renderer>().material.mainTextureOffset = offset;
    }
}
