﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public string[] origins = { "BOREAL", "EUSTACE", "TANJONG", "CENTRAL", "PALMER", "HORACE" };
    public string[] destinations = { "SLOCK", "HEDGE", "PINE", "FLORA", "TACK" };
    public string journeyOrigin, journeyDestination, journeyDate, journeyTime;
    private GameObject trainTicket;

    private int ticketsProcessedCorrectly;
    private int ticketsProcessedIncorrectly;

    public GameObject passengerBeingInspected;

    private RawImage stampCorrect;
    private RawImage stampIncorrect;

    private float timer = 60.0f;

    // Use this for initialization
    void Start () {
        GameObject[] gamePassengers = GameObject.FindGameObjectsWithTag("Passenger");
        foreach (GameObject passenger in gamePassengers)
        {
            passenger.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Passengers/Train_Passenger_" + Random.Range(1, 4).ToString());

            var context = passenger.transform.Find("Context");
            context.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Train_Context");
            context.GetComponent<SpriteRenderer>().enabled = false;

            if (passenger.GetComponent<SpriteRenderer>().flipX)
            {
                context.localPosition = new Vector3(-1.2f, 3.22f, 0.0f);
            }
            else
            {
                context.localPosition = new Vector3(1.2f, 3.22f, 0.0f);
            }
        }

        stampCorrect = GameObject.Find("Canvas/TrainTicket/StampCorrect").GetComponent<RawImage>();
        stampIncorrect = GameObject.Find("Canvas/TrainTicket/StampIncorrect").GetComponent<RawImage>();
        stampCorrect.enabled = false;
        stampIncorrect.enabled = false;

        trainTicket = GameObject.Find("Canvas/TrainTicket");

        journeyOrigin = origins[Random.Range(0, origins.Length -1)];
        journeyDestination = destinations[Random.Range(0, destinations.Length -1)];
        
        int journeyDateDay = Random.Range(1, 30);
        int journeyDateMonth = Random.Range(1, 12);
        journeyDate = generateDate();
        journeyTime = generateTime();

        setJourneyData();

        toggleTrainTicket(false);        
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        GameObject.Find("Canvas/TimeLeft/TimeRemaining").GetComponent<Text>().text = Mathf.Round(timer).ToString();
    }


    public void setJourneyData()
    {
        GameObject.Find("Canvas/CurrentJourney/JourneyOrigin").GetComponent<Text>().text = journeyOrigin;
        GameObject.Find("Canvas/CurrentJourney/JourneyDestination").GetComponent<Text>().text = journeyDestination;
        GameObject.Find("Canvas/CurrentJourney/JourneyDate").GetComponent<Text>().text = journeyDate;
        GameObject.Find("Canvas/CurrentJourney/JourneyTime").GetComponent<Text>().text = journeyTime;

        resetTicketData();

    }

    public void resetTicketData()
    {
        GameObject.Find("Canvas/TrainTicket/TicketOrigin").GetComponent<Text>().text = journeyOrigin;
        GameObject.Find("Canvas/TrainTicket/TicketDestination").GetComponent<Text>().text = journeyDestination;
        GameObject.Find("Canvas/TrainTicket/TicketDate").GetComponent<Text>().text = journeyDate;
        GameObject.Find("Canvas/TrainTicket/TicketTime").GetComponent<Text>().text = journeyTime;
    }

    public void setTicketData(string data, string text)
    {
        GameObject.Find("Canvas/TrainTicket/Ticket" + data).GetComponent<Text>().text = text;
    }

    public string generateDate()
    {
        int journeyDateDay = Random.Range(1, 30);
        int journeyDateMonth = Random.Range(1, 12);
        return (journeyDateDay.ToString().Length < 2 ? "0" + journeyDateDay.ToString() : journeyDateDay.ToString())
            + "-" + (journeyDateMonth.ToString().Length < 2 ? "0" + journeyDateMonth.ToString() : journeyDateMonth.ToString()) + "-2017";
    }

    public string generateTime()
    {
        int journeyTimeHour = Random.Range(7, 23);
        int journeyTimeMinute = Random.Range(0, 59);
        return (journeyTimeHour.ToString().Length < 2 ? "0" + journeyTimeHour.ToString() : journeyTimeHour.ToString()) + ":"
            + (journeyTimeMinute.ToString().Length < 2 ? "0" + journeyTimeMinute.ToString() : journeyTimeMinute.ToString());
    }

    public void toggleTrainTicket(bool active)
    {
        trainTicket.SetActive(active);
    }

    public void processTrainTicket(bool approval)
    {
        var passenger = passengerBeingInspected.GetComponent<Passenger>();
        passenger.ticketChecked = true;

        if (approval)
        {
            Debug.Log("TICKET APPROVED");
        } else
        {
            Debug.Log("TICKET REJECTED");
        }

        if (passenger.problemTicket && approval
            || !passenger.problemTicket && !approval)
        {
            // Fail conditions
            Debug.Log("Ticket incorrectly processed");
            stampIncorrect.enabled = true;
            StartCoroutine(ToggleTicketStamp(stampIncorrect, false, 1));
            ticketsProcessedIncorrectly++;
            GameObject.Find("ticketsProcessedIncorrectly").GetComponent<Text>().text = ticketsProcessedIncorrectly.ToString();
        }
        else
        {
            // Win conditions
            Debug.Log("Ticket processed!");
            stampCorrect.enabled = true;
            StartCoroutine(ToggleTicketStamp(stampCorrect, false, 1));
            ticketsProcessedCorrectly++;
            GameObject.Find("ticketsProcessedCorrectly").GetComponent<Text>().text = ticketsProcessedCorrectly.ToString();
        }

    }

    IEnumerator ToggleTicketStamp(RawImage stamp, bool toggle, int delay)
    {
        print("ToggleComponentEnabled");
        yield return new WaitForSeconds(delay);
        stamp.enabled = toggle;

        Debug.Log("Unlocking controls");
        GameObject.Find("PlayerChar").GetComponentInParent<CharacterController>().enabled = true;
        passengerBeingInspected = null;
        resetTicketData();
        toggleTrainTicket(false);
    }

}
